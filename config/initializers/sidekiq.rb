

params = {
  :url => ENV['REDIS_URL'],
  :namespace => 'blscrapper',
  :concurrency => 10,
  :size => 10
}

Sidekiq.configure_server do |config|
  config.redis = params
end

Sidekiq.configure_client do |config|
  config.redis = params
end

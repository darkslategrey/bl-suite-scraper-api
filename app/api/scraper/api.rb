require 'logging'

module Scraper

  class API < Grape::API
    format :json
    
    resource :campaigns do

      get do
        Campaign.all.as_json
      end

      post do
        logger = Logging.logger['test']
        logger.add_appenders(
                             Logging.appenders.stdout,
                             Logging.appenders.file('example.log')
                             )
        logger.level = :debug
        logger.debug params
        @campaign = Campaign.new
        @campaign.label  = params['label']  if params['label']
        @campaign.inputs = params['inputs'] if params['inputs']
        @campaign.limit  = params['limit']  if params['limit']
        sources = params['sources'] || []
        sources.each do |source|
          source_obj      = Source.new
          source_obj.conf = source['conf']    || {}
          anchors         = source['anchors'] || []
          anchors.each do |anchor|
            anchor_obj      = Anchor.new
            anchor_obj.conf = anchor['conf'] || {}
            source_obj.anchors.push anchor_obj
          end
          @campaign.sources.push source_obj
        end
        @campaign.save
        status 201
        @campaign.as_json
      end
      
    end

    resource :sources do
    end

    resource :anchors do
    end

  end


end

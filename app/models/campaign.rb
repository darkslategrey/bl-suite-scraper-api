
class Campaign
  include Mongoid::Document

  field :label,   type: String
  field :inputs,  type: String
  field :limit,   type: Integer

  embeds_many :sources
end






class Source
  include Mongoid::Document

  field :conf, type: Hash
  embedded_in :campaign 
  embeds_many :anchors
end

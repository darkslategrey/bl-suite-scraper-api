require "test_helper"

# To be handled correctly this spec must end with "Integration Test"
describe "Api Integration Test" do

  it "must gives all the campaigns" do
    get '/api/campaigns'
    res = JSON.parse(response.body)
    response.success?.must_equal true
    res.must_be_instance_of Array
  end


  it "must be possible to create a campaign" do
    campaign = { label: 'campagne1', inputs: 'file:///test.csv', limit: 10 }
    post '/api/campaigns', campaign
    response.success?.must_equal true
    res = JSON.parse response.body
    res['_id'].wont_be_nil
  end


  it "must be possible to create a campaign with sources and anchors" do
    anchor_1 = { conf: { param_1: 'val 5', param_2: 'val_6' } }
    anchor_2 = { conf: { param_1: 'val 7', param_2: 'val_8' } }

    source_1 = { conf: { param_1: 'val 1', param_2: 'val_2' }, anchors: [ anchor_1, anchor_2 ] }
    source_2 = { conf: { param_1: 'val 3', param_2: 'val_4' }, anchors: [ anchor_1, anchor_2 ] }

    campaign = { label: 'campagne1', inputs: 'file:///test.csv', limit: '10', sources: [ source_1, source_2 ] }
    post '/api/campaigns', campaign.to_json, 'CONTENT_TYPE' => 'application/json'
    response.success?.must_equal true
    res = JSON.parse response.body
    res['_id'].wont_be_nil
    res['sources'].must_be_instance_of Array
    res['sources'].size.must_equal 2
    res['sources'][0]['anchors'].must_be_instance_of Array
    res['sources'][0]['anchors'].size.must_equal 2
  end

end
